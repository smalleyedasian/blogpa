$(function () {
    var APPLICATION_ID = "51A5F794-A176-C665-FF62-6282C2698800",
            SECRET_KEY = "8EEB779D-848E-5FEA-FF4E-31B411741700",
            VERSION = "v1";

    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    
    var dataStore = Backendless.Persistence.of(Posts).find();
    var posts = new Posts({title: "pew pew", content:"Hola yo soy Niko", email:"asianperswaysian.13@gmail.com"});
    //dataStore.save(posts);


    var loginScript = $("#login-template").html();
    var loginTemplate = Handlebars.compile(loginScript);

    $('.main-container').html(loginTemplate);

    $(document).on('submit', '.form-signin', function (event) {
        event.preventDefault();

        var data = $(this).serializeArray(),
                email = data[0].value,
                password = data[1].value;

        Backendless.UserService.login(email, password, true, new Backendless.Async(userLogIn, gotError));
    });

    $(document).on('click', '.add-blog', function(){
        var addBlogScript = $("#add-blog-template").html();
        var addBlogTemplate = Handlebars.compile(addBlogScript);

        $('.main-container').html(addBlogTemplate);
    });
});

function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

function userLogIn(user) {
    console.log("You logged in right");

    var welcomeScript = $('#welcome-template').html();
    var welcomeTemplate = Handlebars.compile(welcomeScript);
    var welcomeHTML = welcomeTemplate(user);

    $('.main-container').html(welcomeHTML);
}

function gotError(error) {
    console.log("Error message - " + error.message);
    console.log("Error code -" + error.code);
}

